============
 Lazy Users
============

PROBLEM
=======

You've just joined a team that is developing a new light blogging
platform. It is in its early stages and there are no much features
yet. Users are happy with it but are complaining that they must write
all HTML code themselves. What a shame! Your task is to assist these
lazy users while posting by automatically identifying URLs in the text
and turning them into HTML link tags.

However, remember that users are still allowed to submit HTML
code. You should not touch user's links whatsoever.

TASK
====

Write a program that:

  * Reads the user's input from stdin;
  * Writes the modified text into stdout;

You can assume text is UTF-8 encoded and HTML is well formed.

URLs in our test cases are defined by the following regular expression (POSIX/Perl compatible)::

       (https?://)?([\w\-]+\.)+\w+(/[\w\-_]+)*(/?\?[^ ]*)*

EXAMPLE
=======
  1. in::

        <em>meme.yahoo.com</em>

     out::

        <em><a href="http://meme.yahoo.com">meme.yahoo.com</a></em>

  2. in::

        http://yahoo.com/?rd=http%3A%2F%2Fmeme.yahoo.com

     out::

        <a href="http://yahoo.com/?rd=http%3A%2F%2Fmeme.yahoo.com">http://yahoo.com/?rd=http%3A%2F%2Fmeme.yahoo.com</a>

  2. in::

        <blockquote>
          foobar
          <a href="http://meme.yahoo.com/">
            meme.yahoo.com
          </a>
        </blockquote>

     out::
        
        <blockquote>
          foobar
          <a href="http://meme.yahoo.com/">
            meme.yahoo.com
          </a>
        </blockquote>

  3. in::

        <a href="http://yahoo.com/">
          <strong>Yahoo Meme!</strong>
          <em>meme.yahoo.com</em> is a new light blogging platform!
        </a>

     out::

        <a href="http://yahoo.com/">
          <strong>Yahoo Meme!</strong>
          <em>meme.yahoo.com</em> is a new light blogging platform!
        </a>

SUBMITTING
==========

Create a zip or tar file and send everything you have created to
us. It is also important to write a short text about how to execute
it. Feel free to include any other information that may help us to
understand what you have done.

Here is the list of programming languages you are allowed to use:
  * PHP
  * Python
  * Javascript
  * C
  * C++

Please write a command-line solution. Do not write solutions that depend on a
browser, webserver or IDE to run.
