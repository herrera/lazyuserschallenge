#!/usr/bin/python
# -*- coding: utf-8 -*-

from parser import LazyUserParser
import unittest


class TestLazyUserParser(unittest.TestCase):

	def test_find_url_nodes_01(self):
		parser = LazyUserParser()
		root = parser.load('<em>meme.yahoo.com</em>')
		self.assertEquals(
			['meme.yahoo.com'],
			parser.find_url_nodes(root)
		)

	def test_find_url_nodes_02(self):
		parser = LazyUserParser()
		root = parser.load('http://yahoo.com/?rd=http%3A%2F%2Fmeme.yahoo.com')
		self.assertEquals(
			['http://yahoo.com/?rd=http%3A%2F%2Fmeme.yahoo.com'],
			parser.find_url_nodes(root)
		)

	def test_find_url_nodes_03(self):
		parser = LazyUserParser()
		root = parser.load('<blockquote>foobar<a href="http://meme.yahoo.com/">meme.yahoo.com</a></blockquote>')
		self.assertEquals(
			['meme.yahoo.com'],
			parser.find_url_nodes(root)
		)

	def test_find_url_nodes_04(self):
		parser = LazyUserParser()
		root = parser.load('<a href="http://yahoo.com/"><strong>Yahoo Meme!</strong><em>meme.yahoo.com</em> is a new light blogging platform!</a>')
		self.assertEquals(
			['meme.yahoo.com'],
			parser.find_url_nodes(root)
		)

	def test_can_wrap_01(self):
		parser = LazyUserParser()
		root = parser.load('<em>meme.yahoo.com</em>')
		found = parser.find_url_nodes(root)[0]
		self.assertTrue(parser.can_wrap(found))

	def test_can_wrap_02(self):
		parser = LazyUserParser()
		root = parser.load('http://yahoo.com/?rd=http%3A%2F%2Fmeme.yahoo.com')
		found = parser.find_url_nodes(root)[0]
		self.assertTrue(parser.can_wrap(found))

	def test_can_wrap_03(self):
		parser = LazyUserParser()
		root = parser.load('<blockquote>foobar<a href="http://meme.yahoo.com/">meme.yahoo.com</a></blockquote>')
		found = parser.find_url_nodes(root)[0]
		self.assertFalse(parser.can_wrap(found))

	def test_can_wrap_04(self):
		parser = LazyUserParser()
		root = parser.load('<a href="http://yahoo.com/"><strong>Yahoo Meme!</strong><em>meme.yahoo.com</em> is a new light blogging platform!</a>')
		found = parser.find_url_nodes(root)[0]
		self.assertFalse(parser.can_wrap(found))

	def test_wrap_01(self):
		parser = LazyUserParser()
		root = parser.load('<em>meme.yahoo.com</em>')
		found = parser.find_url_nodes(root)[0]
		parser.wrap(root, found)
		self.assertEquals(
			unicode('<em><a href="http://meme.yahoo.com">meme.yahoo.com</a></em>'),
			unicode(root)
		)

	def test_wrap_02(self):
		parser = LazyUserParser()
		root = parser.load('http://yahoo.com/?rd=http%3A%2F%2Fmeme.yahoo.com')
		found = parser.find_url_nodes(root)[0]
		parser.wrap(root, found)
		self.assertEquals(
			unicode('<a href="http://yahoo.com/?rd=http%3A%2F%2Fmeme.yahoo.com">http://yahoo.com/?rd=http%3A%2F%2Fmeme.yahoo.com</a>'),
			unicode(root)
		)

	def test_wrap_03_root_should_remain_untouched(self):
		parser = LazyUserParser()
		snippet = '<blockquote>foobar<a href="http://meme.yahoo.com/">meme.yahoo.com</a></blockquote>'
		root = parser.load(snippet)
		found = parser.find_url_nodes(root)[0]
		parser.wrap(root, found)
		self.assertEquals(
			unicode(snippet),
			unicode(root)
		)

	def test_wrap_04_root_should_remain_untouched(self):
		parser = LazyUserParser()
		snippet = '<a href="http://yahoo.com/"><strong>Yahoo Meme!</strong><em>meme.yahoo.com</em> is a new light blogging platform!</a>'
		root = parser.load(snippet)
		found = parser.find_url_nodes(root)[0]
		parser.wrap(root, found)
		self.assertEquals(
			unicode(snippet),
			unicode(root)
		)


if __name__ == '__main__':
	unittest.main()

