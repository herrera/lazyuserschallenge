Hi, its a solved Yahoo! code challenge, called 'Lazy Users'.


ABSTRACT
--------

You can see a detailed explanation about its motivations on a doc file
  called 'lazyusers.rst', included in this package root directory.

It was developed with help of Python 2.7.3, a third-party library called
  Beautiful Soup (v 4.1.13) for HTML parsing, which can be found at
  http://crummy.com/software/BeautifulSoup/, and the unittest package
  from Python standard codebase.

Once running 'python parser.py', you could input some plain text or HTML
  snippet, so the script would analyze the need of wrapping the links that
  could be eventually found and, if so, will wrap them with an 'a' tag.


USING
-----

1. You can start the script with a single command on your shell:
  python parser.py

2. Input your plain text or HTML snippet. E.g.:
  <em>yahoo.com<em>

3. The processed input will be shown as the script output. E.g.:
  <em>
   <a href="http://yahoo.com">
    yahoo.com
   </a>
  </em>


TESTING
-------

You can test the script by running the auxiliar script that also
  cames inside this package with the following command:
  python parsertest.py

You should see, as an expected output, something like this:
............
----------------------------------------------------------------------
Ran 12 tests in 0.005s

OK

If you have seen a message like that, it means that everything
  is fine and the script behavior is right.
