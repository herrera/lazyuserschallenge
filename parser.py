#!/usr/bin/python
# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup
from re import compile
from urlparse import urlparse

class LazyUserParser():

	url_regex = compile('(https?://)?([\w\-]+\.)+\w+(/[\w\-_]+)*(/?\?[^ ]*)*')

	def can_wrap(self, node):
		return True if node.parent is None else (node.parent.name.lower() != 'a') and self.can_wrap(node.parent)

	def find_url_nodes(self, node):
		return node.findAll(text = self.url_regex)

	def load(self, html_snippet):
		return BeautifulSoup(html_snippet, 'html.parser')

	def wrap(self, root, node):
		if self.can_wrap(node):
			parsed = urlparse(node)
			url = parsed.geturl() if parsed.scheme != '' else 'http://' + parsed.geturl()
			a_tag = root.new_tag('a', href = url)
			node.wrap(a_tag)
		return node


if __name__ == '__main__':
	snippet = raw_input("Hi Master! Please, feed me with some text: ")
	parser = LazyUserParser()
	root = parser.load(snippet)
	for found in parser.find_url_nodes(root):
		parser.wrap(root, found)
	print "Thanks! Now it looks like:"
	print root.prettify()

